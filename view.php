<?php

function view_rec($id_parent,$data){

    $flag=false;
    foreach ($data as $elem) {              // 'SELECT * FROM categories WHERE id_p='.$id_parent;
                                            // while ($elem=mysql_fetch_assoc)
        if ($elem['id_p']==$id_parent) {
            if (!$flag){
                echo "<ul>\n";
                $flag=true;
            }
            echo "    <li>" . $elem['name'] . "</li>\n";
            view_rec($elem['id'], $data);
            if ($flag){
                echo "</ul>\n";
                $flag=false;
            }
        }
    }
}

view_rec(0,$data); // id=0 - root

